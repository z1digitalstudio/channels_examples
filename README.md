## Setup

```
vagrant up --provision
```

## Use

On one terminal start the django main server
```
vagrant ssh
cd channels_examples && ./run.sh
```

On the browser open 2 tabs with the url:

http://localhost:8010/sse/

click 'Subscribe' and then 'Send'