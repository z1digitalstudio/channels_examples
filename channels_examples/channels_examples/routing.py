# -*- coding: utf-8 -*-

from channels.routing import route

channel_routing = [
    route(
        "http.request",
        "sse_app.consumers.http_sse_susbcribe",
        path=r"^/sse/subscribe/$"
    )
]
