# -*- coding: utf-8 -*-

from channels import Group


def http_sse_susbcribe(message):
    # SSE group
    reply = {
        'status': 200,
        'headers': [('Content-Type', 'text/event-stream'), ],
        'more_content': True
    }
    message.reply_channel.send(reply)
    Group('sse-group').add(message.reply_channel)


def http_sse_publish(msg):
    Group("sse-group").send({
        "content": msg,
        "more_content": True
    })
