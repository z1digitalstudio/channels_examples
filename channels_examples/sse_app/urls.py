# -*- content: utf-8 -*-

from django.conf.urls import url

from .views import SSEexampleTemplateView, SEEexamplePublishEverybody

urlpatterns = [
    url(r'^$', SSEexampleTemplateView.as_view(), name="sse_index"),
    url(r'^publish/$', SEEexamplePublishEverybody.as_view(), name="sse_pub")
]
