# -*- coding: utf-8 -*-

import json

from django.http import HttpResponse
from django.views.generic.base import TemplateView
from django.views.generic import View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from .consumers import http_sse_publish


class SSEexampleTemplateView(TemplateView):
    template_name = "sse_app/index.html"


@method_decorator(csrf_exempt, name='dispatch')
class SEEexamplePublishEverybody(View):
    def gen_sse_message(self, event_type, message_text):
        msg = b'event:' + event_type.encode() + b'\n'
        msg += b'data:' + message_text.encode() + b'\n\n'
        return msg

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body.decode())
        message_text = "{} from {}".format(
            data.get("message"), data.get("guid"))
        encoded_msg = self.gen_sse_message("message", message_text)
        print("CHANNEL NAME: ", request.reply_channel.name)
        http_sse_publish(encoded_msg)
        request.reply_channel.send({
            "content": encoded_msg,
            "more_content": True,
        })
        return HttpResponse("OK")
